#!/usr/bin/env bash

vagrant up --color
sleep 5
script -q /dev/null behave tests
sleep 180
exit_code=$?
vagrant destroy -f
exit $exit_code
