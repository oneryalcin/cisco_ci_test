from behave import given, then
from Router import Router

@given(u'I configure "{interface}" and enable')
def step_impl(context, interface):
        context.router = Router()
        context.router.interface = interface


@then(u'interface should be up and ip address must be "{prefix}"')
def step_impl(context, prefix):
    itf = context.router.interface
    prefix_ = context.router.get_interface_ip(itf)
    assert prefix == prefix_  
