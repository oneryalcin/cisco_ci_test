from napalm.base import get_network_driver
import pprint
pp = pprint.PrettyPrinter(indent=4)


class Router:

    def __init__(self):
        self.driver = get_network_driver('ios')
        self.config = {
            'hostname': '127.0.0.1',
            'username': 'vagrant',
            'password': '',
            'optional_args': {
                'port': 12202,
                'key_file': '/Users/mya03/.vagrant.d/insecure_private_key'}
        }

    def get_interface_ip(self, interface):
        with self.driver(**self.config) as router:
            interface_ips = router.get_interfaces_ip()

        prefix = interface_ips[interface].get('ipv4')
        ip = prefix.keys()[0]
        subnet = prefix[ip].get('prefix_length')
        return('{}/{}'.format(ip, subnet))
