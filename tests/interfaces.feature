Feature: Validate Interface configs

    Scenario Outline: Interfaces are up and addresses are configured
    Given I configure "<interface>" and enable
    Then interface should be up and ip address must be "<prefix>"

 Examples: RouteServer
   | interface         | prefix     |
   | GigabitEthernet2  | 2.2.2.1/24 |
